﻿using BrandedFareData.Entity;
using TinyCsvParser.Mapping;
using TinyCsvParser.TypeConverter;

namespace BrandedFareImport.Mapping
{
    public class AncillaryBrandCsvMapping : CsvMapping<Ancillary>
    {
        public AncillaryBrandCsvMapping(ITypeConverterProvider typeConverterProvider) : base(typeConverterProvider)
        {
            MapProperty(0, x => x.ServiceID);
            MapProperty(1, x => x.CarrierCode);
            MapProperty(2, x => x.ServiceGroupCode);
            MapProperty(3, x => x.ServiceSubGroupCode);
            MapProperty(4, x => x.ServiceSubCode);
            MapProperty(5, x => x.ServiceTagName);
            MapProperty(6, x => x.ServiceCommercialAncillaryName);
            MapProperty(7, x => x.ServiceShortName);
            MapProperty(8, x => x.ServiceExternalName);
            MapProperty(9, x => x.ServiceStrapLine);
            MapProperty(10, x => x.NewServiceIndicator);
            MapProperty(11, x => x.ServiceTypeCode);
            MapProperty(12, x => x.ServiceEquipmentTypeCode);
            MapProperty(13, x => x.ServiceCabinCode);
            MapProperty(14, x => x.ServiceDescription1);
            MapProperty(15, x => x.ServiceDescription2);
            MapProperty(16, x => x.ServiceDescription3);
            MapProperty(17, x => x.ServiceDescription4);
            MapProperty(18, x => x.ServiceEffectiveDate, new DateTimeConverter("dd/MM/yyyy"));
            MapProperty(19, x => x.ServiceDiscontinuedDate, new DateTimeConverter("dd/MM/yyyy"));
            MapProperty(20, x => x.ServiceConsumerImageLocation);
            MapProperty(21, x => x.ServiceConsumerText);
            MapProperty(22, x => x.LocationOneTypeCode);
            MapProperty(23, x => x.LocationOne);
            MapProperty(24, x => x.LocationTwoTypeCode);
            MapProperty(25, x => x.LocationTwo);
            MapProperty(26, x => x.PermittedIndicator);
            MapProperty(27, x => x.DirectionApplicationCode);
            MapProperty(28, x => x.GlobalIndicator);
            //MapProperty(29, x => x.AD);
        }
    }
}