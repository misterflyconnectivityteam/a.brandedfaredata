﻿using BrandedFareData.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace BrandedFareImport.Store
{
    public interface IStoreBrandedFare
    {
        void InsertFamilies(IEnumerable<Family> families);
    }
}
