﻿using BrandedFareData.Entity;
using BrandedFareImport.Manager;
using BrandedFareSearch.Search;
using BrandedFareWebApi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BrandedFareWebApi.Services
{
    public class BrandService : IBrandService
    {
        private readonly IManager<Brand> brandManager;
        private readonly IManager<Family> familyManager;
        private readonly IManager<Ancillary> ancillaryManager;
        private readonly IManager<Zone> zoneManager;

        private readonly IEnumerable<Brand> Brands;
        private readonly IEnumerable<Family> Families;
        private readonly IEnumerable<Ancillary> Ancillaries;
        private readonly IEnumerable<Zone> Zones;

        public BrandService(IManager<Family> familyManager, IManager<Brand> brandManager, IManager<Ancillary> ancillaryManager, IManager<Zone> zoneManager)
        {
            this.brandManager = brandManager;

            this.Brands = this.brandManager.LoadFromFile();

            this.familyManager = familyManager;

            this.Families = this.familyManager.LoadFromFile();

            this.ancillaryManager = ancillaryManager;

            this.Ancillaries = this.ancillaryManager.LoadFromFile();

            this.zoneManager = zoneManager;

            this.Zones = this.zoneManager.LoadFromFile();
        }

        public IEnumerable<Brand> Get(int familyID)
        {
            if (familyID == 0)
                return null;

            return Brands.Where(b => b.FareFamilyID == familyID)
                .OrderBy(b => b.BrandSequenceNumber);
        }

        public Brand Find(int brandKey)
        {
            if (brandKey == 0)
                return null;

            Console.WriteLine($"Brand Nb  {Brands.Count()}");

            return Brands.FirstOrDefault(b => b.BrandKey == brandKey);
        }

        public IEnumerable<ServiceData> FindIncludedServices(int brandKey)
        {
            if (brandKey == 0)
                return null;

            return Find(brandKey)?.IncludedServiceData;
        }

        public IEnumerable<ServiceData> FindChargeableServices(int brandKey)
        {
            if (brandKey == 0)
                return null;

            return Find(brandKey)?.ChargeableServiceData;
        }

        public IEnumerable<ServiceData> FindExcludedServices(int brandKey)
        {
            if (brandKey == 0)
                return null;

            return Find(brandKey)?.ExcludedServiceData;
        }

        public IEnumerable<Family> Get()
        {
            var result = Families;

            return result;
        }

        public Family Find(string provider, Travel travel)
        {
            if (!CheckData(travel))
                return null;

            var matchFamily = new MatchFamily(new SearchFamily(this.Families));

            //--> step 1 carrier
            matchFamily.SeachByCarrier(travel.Carrier);

            //--> step 2 search date
            matchFamily.SearchBySearchDate(DateTime.Now);

            //--> step 3 travel dates
            matchFamily.SearchByTravelDates(travel);

            //--> step 4 Passenger Type Codes
            matchFamily.SearchByPassengerTypeCodes(travel?.Passengers?.Select(p => p.PTC).Distinct());

            //--> step 5 Account Codes
            matchFamily.SearchByAccountCodes(travel);

            //--> step 6 Flight Geography
            matchFamily.SearchByFlightGeography(travel);

            IEnumerable<Family> result = matchFamily.Families;

            if (result == null || !result.Any())
                return null;

            var orderedFamilies = result.OrderBy(r => r.SequenceNumber);

            return orderedFamilies.FirstOrDefault();
        }

        private bool CheckData(Travel travel)
        {
            if (travel?.Itineraries == null && travel?.Passengers == null)
                return false;

            return true;
        }

        public IEnumerable<Ancillary> FindAncillaries(int brandKey)
        {
            if (brandKey == 0)
                return null;

            var brand = Find(brandKey);

            if (brand == null)
                return null;

            var serviceIDs = brand.BrandChargeableServiceID.Concat(brand.BrandExcludedServiceID).Concat(brand.BrandIncludedServiceID);

            return Ancillaries.Where(a => serviceIDs.Contains(a.ServiceID.ToString()));

        }

        public Ancillary FindAncillary(int serviceID)
        {
            if (serviceID == 0)
                return null;

            if (Ancillaries == null || !Ancillaries.Any())
                return null;

            return Ancillaries.FirstOrDefault(a => a.ServiceID == serviceID);

        }

        public IEnumerable<Ancillary> FindIncludedAncillaries(int brandKey)
        {
            var includedServices = FindIncludedServices(brandKey);

            if (includedServices == null || !includedServices.Any())
                return null;

            var includedAncillaries = from a in Ancillaries
                                      join s in includedServices
                                      on a.ServiceID.ToString() equals s.ServiceID
                                      select a;

            return includedAncillaries;
        }

        public IEnumerable<Ancillary> FindChargeableAncillaries(int brandKey)
        {
            var chargeableServices = FindChargeableServices(brandKey);

            if (chargeableServices == null || !chargeableServices.Any())
                return null;

            var chargeableAncillaries = from a in Ancillaries
                                        join s in chargeableServices
                                        on a.ServiceID.ToString() equals s.ServiceID
                                        select a;

            return chargeableAncillaries;
        }

        public IEnumerable<Ancillary> FindExcludedAncillaries(int brandKey)
        {
            var excludedServices = FindExcludedServices(brandKey);

            if (excludedServices == null || !excludedServices.Any())
                return null;

            var excludedAncillaries = from a in Ancillaries
                                      join s in excludedServices
                                      on a.ServiceID.ToString() equals s.ServiceID
                                      select a;

            return excludedAncillaries;
        }
    }
}