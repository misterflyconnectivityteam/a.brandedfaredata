﻿using BrandedFareData.Entity;
using BrandedFareWebApi.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;

namespace BrandedFareWebApi.Controllers
{
    [ApiController]
    [Produces("application/json")]
    //[ApiVersion("1.0")]
    [Route("/api/v{version:apiVersion}/[controller]")]
    public class ServicesController : ControllerBase
    {

        private readonly string LogText = "[BrandedFareWebApi IncludedServiceController]";
        private readonly IBrandService brandService;

        public ServicesController(IBrandService brandService)
        {
            this.brandService = brandService;
        }

        /// <summary>
        /// Get Brands
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("IncludedServices/{brandKey}", Name = "IncludedServices")]
        //[MapToApiVersion("1.0")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<ServiceData>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(List<ServiceData>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(List<ServiceData>), StatusCodes.Status500InternalServerError)]
        public ActionResult<IEnumerable<ServiceData>> IncludedServices(int brandKey)
        {
            //-->
            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine($"{LogText} GetCities() | START MAIN | TIME: {sw.Elapsed}");

            try
            {
                sw.Stop();
                //Console.WriteLine($"{LogText} GetCities() | END | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()}");

                if (brandKey == -1)
                    return null;

                return new OkObjectResult(brandService.FindIncludedServices(brandKey));
                //return StatusCode(StatusCodes.Status200OK, data.Item2);
            }
            catch (Exception ex)
            {
                //Console.WriteLine($"{LogText} GetCities() | END-MAIN-ERROR | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()} | ERROR: {ex}");
                //return StatusCode(StatusCodes.Status500InternalServerError, new List<City>());
            }
            return null;
        }

        /// <summary>
        /// Get Brands
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("ExcludedServices/{brandKey}", Name = "ExcludedServices")]
        //[MapToApiVersion("1.0")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<ServiceData>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(List<ServiceData>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(List<ServiceData>), StatusCodes.Status500InternalServerError)]
        public ActionResult<IEnumerable<ServiceData>> ExcludedServices(int brandKey)
        {
            //-->
            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine($"{LogText} GetCities() | START MAIN | TIME: {sw.Elapsed}");

            try
            {
                sw.Stop();
                //Console.WriteLine($"{LogText} GetCities() | END | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()}");

                if (brandKey == -1)
                    return null;

                return new OkObjectResult(brandService.FindExcludedServices(brandKey));
                //return StatusCode(StatusCodes.Status200OK, data.Item2);
            }
            catch (Exception ex)
            {
                //Console.WriteLine($"{LogText} GetCities() | END-MAIN-ERROR | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()} | ERROR: {ex}");
                //return StatusCode(StatusCodes.Status500InternalServerError, new List<City>());
            }
            return null;
        }

        /// <summary>
        /// Get Brands
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("ChargeableServices/{brandKey}", Name = "ChargeableServices")]
        //[MapToApiVersion("1.0")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<ServiceData>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(List<ServiceData>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(List<ServiceData>), StatusCodes.Status500InternalServerError)]
        public ActionResult<IEnumerable<ServiceData>> ChargeableServices(int brandKey)
        {
            //-->
            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine($"{LogText} GetCities() | START MAIN | TIME: {sw.Elapsed}");

            try
            {
                sw.Stop();
                //Console.WriteLine($"{LogText} GetCities() | END | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()}");

                if (brandKey == -1)
                    return null;

                return new OkObjectResult(brandService.FindChargeableServices(brandKey));
                //return StatusCode(StatusCodes.Status200OK, data.Item2);
            }
            catch (Exception ex)
            {
                //Console.WriteLine($"{LogText} GetCities() | END-MAIN-ERROR | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()} | ERROR: {ex}");
                //return StatusCode(StatusCodes.Status500InternalServerError, new List<City>());
            }
            return null;
        }


    }
}