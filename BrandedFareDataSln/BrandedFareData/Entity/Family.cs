﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BrandedFareData.Entity
{
    public class Family : BaseEntity
    {
        public int ID { get; set; }
        public string CarrierCode { get; set; }
        public string FareFamilyName { get; set; }
        public string FareFamilyNameSearch { get; set; }
        public string NewFamilyIndicator { get; set; }

        public string PassengerTypeCode { get; set; }

        public DateTime EffectiveSearchDate { get; set; }
        public DateTime DiscontinueSearchDate { get; set; }
        public string PublicIndicator { get; set; }
        public DateTime EffectiveTravelDate { get; set; }
        public DateTime DiscontinueTravelDate { get; set; }
        public int SequenceNumber { get; set; }
        
        public string ATPCOProgramCode { get; set; }
        public string ATPCOProgramDescription { get; set; }
        public string ATPCOSequenceNumber { get; set; }
        public int VersionNumber { get; set; }
        public string SourceType { get; set; }
        public string Status { get; set; }

        public string AccountTypeCode { get; set; }
        public string GlobalIndicator { get; set; }

        public string[] IATA { get; set; }
        public string[] GDS { get; set; }

        public string[] PCC { get; set; }

        public string[] SecurityLocationTypeCode { get; set; }

        public string[] SecurityLocationType { get; set; }

        public string[] PermittedInd { get; set; }

        public string[] LocationOneTypeCode { get; set; }
        public string[] LocationOne { get; set; }

        public string[] LocationTwoTypeCode { get; set; }

        public string[] LocationTwo { get; set; }

        public string[] PermittedIndicator { get; set; }

        public string[] DirectionApplicationCode { get; set; }

        public IEnumerable<PointOfSaleRules> PointOfSaleRules
        {
            get
            {
                if (this.IATA == null || !this.IATA.Any())
                    return new List<PointOfSaleRules>();

                return this.IATA.Select((data, idx) => new PointOfSaleRules
                {
                    IATA = data,
                    GDS = GetArrayValue(GDS, idx),
                    PCC = GetArrayValue(PCC, idx),
                    SecurityLocationTypeCode = GetArrayValue(SecurityLocationTypeCode, idx),
                    SecurityLocationType = GetArrayValue(SecurityLocationType, idx),
                    PermittedInd = GetArrayValue(PermittedInd, idx),
                });
            }
        }

        public IEnumerable<FlightGeographyRules> FlightGeographyRules
        {
            get
            {

                string[] refData = LocationOneTypeCode;

                if (LocationOne.Length > refData.Length)
                    refData = LocationOne;
                if (LocationTwoTypeCode.Length > refData.Length)
                    refData = LocationTwoTypeCode;
                if (LocationTwo.Length > refData.Length)
                    refData = LocationTwo;
                if (PermittedIndicator.Length > refData.Length)
                    refData = PermittedIndicator;
                if (DirectionApplicationCode.Length > refData.Length)
                    refData = DirectionApplicationCode;

                if (refData == null || !refData.Any())
                    return new List<FlightGeographyRules>();

                return refData.Select((data, idx) => new FlightGeographyRules
                {
                    LocationOneTypeCode = GetArrayValue(LocationOneTypeCode, idx),
                    LocationOne = GetArrayValue(LocationOne, idx),
                    LocationTwoTypeCode = GetArrayValue(LocationTwoTypeCode, idx),
                    LocationTwo = GetArrayValue(LocationTwo, idx),
                    PermittedIndicator = GetArrayValue(PermittedIndicator, idx),
                    DirectionApplicationCode = GetArrayValue(DirectionApplicationCode, idx),
                });
            }
        }
    }
}