﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrandedFareImport.Tokenizer
{
    class PointOfSaleTokenize
    {
        public readonly char[] FieldsSeparator;
        public readonly bool TrimLine;

        public PointOfSaleTokenize(char[] fieldsSeparator, bool trimLine)
        {
            FieldsSeparator = fieldsSeparator;
            TrimLine = trimLine;
        }

        public string[] Tokenize(string input)
        {
            if (TrimLine)
            {
                return input.Trim().Split(FieldsSeparator);
            }
            return input.Split(FieldsSeparator);
        }

        public override string ToString()
        {
            return string.Format("StringSplitTokenizer (FieldsSeparator = {0}, TrimLine = {1})", FieldsSeparator, TrimLine);
        }
    }
}
