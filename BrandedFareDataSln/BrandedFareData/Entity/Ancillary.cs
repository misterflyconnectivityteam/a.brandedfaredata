﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BrandedFareData.Entity
{
    public class Ancillary : BaseEntity
    {
        public int ServiceID { get; set; }
        public string CarrierCode { get; set; }
        public string ServiceGroupCode { get; set; }
        public string ServiceSubGroupCode { get; set; }
        public string ServiceSubCode { get; set; }

        public string ServiceTagName { get; set; }

        public string ServiceCommercialAncillaryName { get; set; }
        public string ServiceShortName { get; set; }
        public string ServiceExternalName { get; set; }
        public string ServiceStrapLine { get; set; }
        public string NewServiceIndicator { get; set; }

        public string ServiceTypeCode { get; set; }

        public string ServiceEquipmentTypeCode { get; set; }
        public string ServiceCabinCode { get; set; }
        public string ServiceDescription1 { get; set; }
        public string ServiceDescription2 { get; set; }
        public string ServiceDescription3 { get; set; }
        public string ServiceDescription4 { get; set; }
        public DateTime ServiceEffectiveDate { get; set; }
        public DateTime ServiceDiscontinuedDate { get; set; }
        public string ServiceConsumerImageLocation { get; set; }
        public string ServiceConsumerText { get; set; }
        public string[] LocationOneTypeCode { get; set; }
        public string[] LocationOne { get; set; }
        public string[] LocationTwoTypeCode { get; set; }
        public string[] LocationTwo { get; set; }
        public string[] PermittedIndicator { get; set; }

        public string[] DirectionApplicationCode { get; set; }

        public string GlobalIndicator { get; set; }
        public string AD { get; set; }

        public IEnumerable<FlightGeographyRules> FlightGeographyRules
        {
            get
            {

                string[] refData = LocationOneTypeCode;

                if (LocationOne.Length > refData.Length)
                    refData = LocationOne;
                if (LocationTwoTypeCode.Length > refData.Length)
                    refData = LocationTwoTypeCode;
                if (LocationTwo.Length > refData.Length)
                    refData = LocationTwo;
                if (PermittedIndicator.Length > refData.Length)
                    refData = PermittedIndicator;
                if (DirectionApplicationCode.Length > refData.Length)
                    refData = DirectionApplicationCode;

                if (refData == null || !refData.Any())
                    return new List<FlightGeographyRules>();

                return refData.Select((data, idx) => new FlightGeographyRules
                {
                    LocationOneTypeCode = GetArrayValue(LocationOneTypeCode, idx),
                    LocationOne = GetArrayValue(LocationOne, idx),
                    LocationTwoTypeCode = GetArrayValue(LocationTwoTypeCode, idx),
                    LocationTwo = GetArrayValue(LocationTwo, idx),
                    PermittedIndicator = GetArrayValue(PermittedIndicator, idx),
                    DirectionApplicationCode = GetArrayValue(DirectionApplicationCode, idx),
                });
            }
        }
    }
}