﻿using System.Linq;

namespace BrandedFareImport.Manager
{
    public class LanguageParse : ILanguageParse
    {
        public string ParseLanguageCode(string fileLocation)
        {

            if (string.IsNullOrWhiteSpace(fileLocation))
                return "EN";

            if (!fileLocation.Contains("-"))
                return "EN";

            var items = fileLocation.Split('-');

            if (!items.Any())
                return "EN";

            var result = items[0].ToUpper();

            return result;
        }
    }
}
