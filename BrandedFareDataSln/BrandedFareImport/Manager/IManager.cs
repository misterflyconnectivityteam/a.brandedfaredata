﻿using BrandedFareData.Entity;
using System.Collections.Generic;

namespace BrandedFareImport.Manager
{
    public interface IManager<T> where T: class
    {
        IEnumerable<T> LoadFromFile();
    }
}
