﻿using BrandedFareImport.Manager;
using System;
using System.Linq;

namespace BrandedFareImportClient
{
    class Program
    {
        static void Main(string[] args)
        {

            var family = new FamilyManager("Family.csv", ',', new LanguageParse());

            Console.WriteLine(family.CsvConfigDisplay);

            var result = family.LoadFromFile().ToList();

            var brand = new BrandManager("Brand.csv", ',', new LanguageParse());

            Console.WriteLine(brand.CsvConfigDisplay);

            var result2 = brand.LoadFromFile().ToList();

            var ancillary = new AncillaryManager("Ancillary.csv", ',', new LanguageParse());

            Console.WriteLine(ancillary.CsvConfigDisplay);

            var result3 = ancillary.LoadFromFile().ToList();

        }
    }
}
