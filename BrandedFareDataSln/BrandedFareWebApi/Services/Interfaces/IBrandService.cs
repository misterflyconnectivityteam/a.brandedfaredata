﻿using BrandedFareData.Entity;
using BrandedFareSearch.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandedFareWebApi.Services.Interfaces
{
    public interface IBrandService
    {
        IEnumerable<Family> Get();
        IEnumerable<Brand> Get(int familyID);
        
        Brand Find(int brandKey);
        IEnumerable<ServiceData> FindIncludedServices(int brandKey);
        IEnumerable<ServiceData> FindChargeableServices(int brandKey);
        IEnumerable<ServiceData> FindExcludedServices(int brandKey);

        IEnumerable<Ancillary> FindIncludedAncillaries(int brandKey);
        IEnumerable<Ancillary> FindChargeableAncillaries(int brandKey);
        IEnumerable<Ancillary> FindExcludedAncillaries(int brandKey);

        Family Find(string provider,  Travel travel);

        IEnumerable<Ancillary> FindAncillaries(int brandKey);

        Ancillary FindAncillary(int serviceID);
    }

   
}
