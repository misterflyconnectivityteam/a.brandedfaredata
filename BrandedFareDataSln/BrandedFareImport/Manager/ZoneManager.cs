﻿using BrandedFareData.Entity;
using BrandedFareImport.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinyCsvParser;
using TinyCsvParser.Mapping;
using TinyCsvParser.Tokenizer.RFC4180;
using TinyCsvParser.TypeConverter;

namespace BrandedFareImport.Manager
{
    public class ZoneManager : IManager<Zone>
    {
        private readonly string fileLocation;

        private readonly int degreeOfParallelism = 10;
        private readonly bool keepOrder = false;

        public string CsvConfigDisplay;
        private readonly ILanguageParse languageParse;

        public IEnumerable<Zone> Zones { get; set; }

        public ZoneManager(string fileLocation, char separator)
        {
            // Use a " as Quote Character, a \\ as Escape Character and a , as Delimiter.
            var options = new Options('"', '\\', separator);

            // Initialize the Rfc4180 Tokenizer:
            var tokenizer = new RFC4180Tokenizer(options);

            CsvMapping = new ZoneCsvMapping(new StringArrayTypeConverterProvider());
            CsvParser = new CsvParser<Zone>(new CsvParserOptions(true, null, tokenizer, degreeOfParallelism, keepOrder), CsvMapping);

            CsvConfigDisplay = CsvParser.ToString();

            this.fileLocation = fileLocation;            
        }

        public ICsvMapping<Zone> CsvMapping { get; set; }
        public ICsvParser<Zone> CsvParser { get; set; }


        public IEnumerable<Zone> LoadFromFile()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(this.fileLocation))
                    return null;
                else if (!System.IO.File.Exists(this.fileLocation))
                    return null;
                else if (CsvParser == null)
                    return null;

                CsvReaderOptions csvReaderOptions = new CsvReaderOptions(new[] { "\r\n" });

                var contentFile = System.IO.File.ReadAllText(fileLocation);

                if (contentFile == null)
                    return null;

                //--> We don't use ReadFromFile because it does not handle the case newline inside "" : "    newline    "
                var result = (CsvParser as CsvParser<Zone>)?.ReadFromString(csvReaderOptions, contentFile);


                if (result == null)
                    return null;
                else
                {
                    var zones = result.Where(i => i.IsValid)
                          .Select(i => i.Result).ToList();

                    
                    return zones;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
