﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandedFareData.Entity;
using BrandedFareImport.Mapping;
using TinyCsvParser;
using TinyCsvParser.Mapping;
using TinyCsvParser.Tokenizer;
using TinyCsvParser.Tokenizer.RFC4180;
using TinyCsvParser.TypeConverter;

namespace BrandedFareImport.Manager
{
    public class BrandManager : IManager<Brand>
    {
        private readonly string fileLocation;
        
        private readonly int degreeOfParallelism = 10;
        private readonly bool keepOrder = false;

        public string CsvConfigDisplay;
        private readonly ILanguageParse languageParse;

        public BrandManager(string fileLocation, char separator, ILanguageParse languageParse)
        {
            // Initialize the Rfc4180 Tokenizer:
            var tokenizer = new QuotedStringTokenizer('"', '\\', separator);

            CsvMapping = new BrandCsvMapping(new StringArrayTypeConverterProvider());
            CsvParser = new CsvParser<Brand>(new CsvParserOptions(true, null, tokenizer,  degreeOfParallelism, keepOrder), CsvMapping);

            CsvConfigDisplay = CsvParser.ToString();

            this.fileLocation = fileLocation;
            this.languageParse = languageParse;
        }

        public ICsvMapping<Brand> CsvMapping { get; set; }
        public ICsvParser<Brand> CsvParser { get; set; }

        public IEnumerable<Brand> LoadFromFile()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(this.fileLocation))
                    return null;
                else if (!System.IO.File.Exists(this.fileLocation))
                    return null;
                else if (CsvParser == null)
                    return null;

                CsvReaderOptions csvReaderOptions = new CsvReaderOptions(new[] { "\r\n"});

                var contentFile = System.IO.File.ReadAllText(fileLocation);

                if (contentFile == null)
                    return null;

                //--> We don't use ReadFromFile because it does not handle the case newline inside "" : "    newline    "
                var result = (CsvParser as CsvParser<Brand>)?.ReadFromString(csvReaderOptions, contentFile);

                var languageCode = languageParse.ParseLanguageCode(this.fileLocation);

                if (result == null)
                    return null;
                else
                {
                    Console.WriteLine($"Brands Loaded {result.Count()}");

                    var nbbrands = result.Count();

                    var brands = result.Where(i => i.IsValid)
                          .Select(i => i.Result).ToList();

                    brands.ForEach(f => f.LanguageCode = languageCode);

                    return brands;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
