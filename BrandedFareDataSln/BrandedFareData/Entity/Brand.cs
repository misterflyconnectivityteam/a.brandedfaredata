﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BrandedFareData.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class Brand : BaseEntity
    {
        public int FareFamilyID { get; set; }
        public int BrandKey { get; set; }
        public string CarrierCode { get; set; }

        public string BrandSequenceNumber { get; set; }

        public string BrandRuleText { get; set; }

        #region === Brand Info ===

        public string BrandNameTitle { get; set; }

        public string BrandExternalName { get; set; }
        public string BrandShortName { get; set; }

        public string BrandStrapLine { get; set; }

        public string BrandConsumerImageLocation { get; set; }

        public string BrandUpsellImageLocation { get; set; }

        public string BrandUpsellText { get; set; }

        public string BrandConsumerText { get; set; }

        #endregion

        #region === Including/excluding Data ===

        /// <summary>
        /// A brand code is used by Low Cost/API carriers to identify their brand and should be received
        /// along with the fare from these carriers.
        /// You may receive one or more brand codes in these fields per brand(e.g., BU and KT meaning at
        /// least one of these must be applicable to the fare).
        /// When a brand code is received in the ‘Brand Code’ field, for the brand to match the fare, the
        /// same brand code must be received for the fare in question.
        /// </summary>
        public string BrandCode { get; set; }

        /// <summary>
        /// When a brand code is received in the ‘Brand CodeExcluding’ field, for the brand to match the
        /// fare, the same brand code must not be received for the fare in question.
        /// </summary>
        public string BrandCodeExcluding { get; set; }

        /// <summary>
        /// When a brand code is received in the ‘Brand CodeExcluding’ field, for the brand to match the
        /// fare, the same brand code must not be received for the fare in question.
        /// When a flight range is received in the ‘Brand Matching FlightRanges’ field, for the brand to
        /// match the fare, the flight number(s) for this fare must be within the range specified.
        /// When multiple flight ranges are received in the ‘Brand Matching FlightRanges’ field, for the
        /// brand to match the fare, the flight number(s) for this fare must be within any of the range
        /// specified.
        /// </summary>
        public string BrandMatchingFlightRanges { get; set; }

        /// <summary>
        /// Flight numbers are used to match brands only to specific flight numbers.
        /// You may receive one or more flight numbers in these fields per brand(e.g., 11; 13; 27 meaning
        /// only these specific flight numbers for this carrier).
        /// When a flight number is received in the ‘Brand Matching FlightNumbers’ field, for the brand to
        /// match the fare, the flight number for this fare must be the same as the flight number specified.
        /// When multiple flight numbers are received in the ‘Brand Matching FlightNumbers’ field, for the
        /// brand to match the fare, the flight number/s for this fare must be the same as those specified
        /// </summary>

        public string BrandMatchingFlightNumbers { get; set; }

        /// <summary>
        /// Full or partial fare basis codes are used to match brands to specific fare basis codes or partial
        /// fare basis codes.
        /// You may receive one or more full or partial fare basis codes as matching or excluding criteria
        /// per brand.
        /// Along with letters and numbers, a hyphen (-) can be received as part of the fare basis code.
        /// Where a hyphen (-) is found, this indicates a ‘Wildcard’, meaning that any character can take
        /// the place of the wildcard when matching the fare basis code of the fare to the data on the file.
        /// 
        /// JFLXX = A full fare basis code
        /// As no hyphens are included, the fare basis code for the fare received must match this fare basis
        /// code, with no extra characters either before or after the full code of ‘JFLXX’.
        /// 
        /// JFLXX- = A partial fare basis code with wildcard at the end
        /// The fare basis code includes a hyphen at the end, so this means that the fare basis code must
        /// begin with JFLXX but must include another character(or multiple other characters) at the end
        /// of the fare basis code.
        /// 
        /// -JFLXX- = A partial fare basis code with wildcards at the beginning and end of the code
        /// The fare basis code includes a hyphen at the beginning and end, this means that the fare basis
        /// code must begin with one or more characters before JFLXX, and there must be one or more
        /// characters after JFLXX.
        /// 
        /// JFL-XX = A partial fare basis code with wildcard within the code
        /// The fare basis includes a hyphen within the fare basis provided, this means that there must be
        /// one character(alpha or numeric) after the JFL and before the XX.As no hyphens are found at
        /// the beginning or end, the fare basis must begin with JFL and end in XX.

        /// When a single fare basis code is received in the ‘Brand Matching FareBasisCode’ field, for the
        /// brand to match the fare, the fare basis for this fare must match the fare basis code received in
        /// the data file.
        ///  When multiple fare basis codes are received in the ‘Brand Matching FareBasisCode’ field, for
        /// the brand to match the fare, the fare basis code of the fare must match at least one of the fare
        /// basis codes received in the data file.
        /// </summary>

        public string[] BrandMatchingFareBasisCode { get; set; }

        /// <summary>
        /// Fare type codes can be used to match brands to specific fare types. You may receive one or
        /// more fare type codes as matching or excluding criteria per brand.
        /// 
        /// When a single fare type code is received in the ‘Brand Matching FareTypeCode’ field, for the
        /// brand to match the fare, the fare type code for this fare must match the fare type code
        /// received in the data file.
        /// When multiple fare type codes are received in the ‘Brand Matching FareTypeCode’ field, for the
        /// brand to match the fare, the fare type code of the fare must match at least one of the fare type
        /// codes received in the data file.
        /// </summary>
        public string[] BrandMatchingFareTypeCode { get; set; }

        /// <summary>
        /// RBD stands for ‘Reservation Booking Designator’ and is the one-letter booking class for the fare.
        /// You may receive one or more RBDs as matching or excluding criteria per brand.
        /// When a single RBD is received in the ‘Brand Matching RBD’ field, for the brand to match the
        /// fare, the RBD for this fare must match the RBD received in the data file.
        /// When multiple RBDs are received in the ‘Brand Matching RBD’ field, for the brand to match the
        /// fare, the RBD of the fare must match at least one of the fare type codes received in the data
        /// file.
        /// </summary>
        public string[] BrandMatchingReservationBookingDesignor { get; set; }

        /// <summary>
        /// Tariff codes can be used to match brands to specific fare tariffs. You may receive one or more
        /// tariffs as matching or excluding criteria per brand.
        /// When a single tariff is received in the ‘Brand Matching Tariff’ field, for the brand to match the
        /// fare, the tariff for this fare must match the tariff received in the data file.
        /// When multiple tariffs are received in the ‘Brand Matching Tariff’ field, for the brand to match
        /// the fare, the tariff of the fare must match at least one of the tariff codes received in the data
        /// file.
        /// </summary>
        public string[] BrandMatchingTariff { get; set; }

        /// <summary>
        /// Rule codes can be used to match brands to specific fare rules.You may receive one or more
        /// rules as matching or excluding criteria per brand.
        /// When a single rule is received in the ‘Brand Matching Rule’ field, for the brand to match the
        /// fare, the rule for this fare must match the rule received in the data file.
        /// When multiple rules are received in the ‘Brand Matching Rule’ field, for the brand to match the
        /// fare, the rule of the fare must match at least one of the rule codes received in the data file.
        /// </summary>
        public string[] BrandMatchingRule { get; set; }

        /// <summary>
        /// When a flight number is received in the ‘Brand Excluding FlightNumbers’ field, for the brand to
        /// match the fare, the flight number for this fare must not be the flight number specified.
        /// When multiple flight numbers are received in the ‘Brand Excluding FlightNumbers’ field, for the
        /// brand to match the fare, the flight number(s) for this fare must not be any of the flight numbers
        /// specified.
        /// </summary>
        public string BrandExcludingFlightNumbers { get; set; }

        /// <summary>
        /// When a flight range is received in the ‘Brand Excluding FlightRanges’ field, for the brand to
        /// match the fare, the flight number(s) for this fare must not be within the range specified.
        /// When multiple flight ranges are received in the ‘Brand Matching FlightRanges’ field, for the
        /// brand to match the fare, the flight number(s) for this fare must not be within any of the range
        /// specified.
        /// </summary>
        public string BrandExcludingFlightRanges { get; set; }

        /// <summary>
        /// When a single fare basis code is received in the ‘Brand Excluding FareBasisCode’ field, for the
        /// brand to match the fare, the fare basis code for this fare must not match the fare basis received
        /// in the data file.
        /// When multiple fare basis codes are received in the ‘Brand Excluding FareBasisCode’ field, for
        /// the brand to match the fare, the fare basis code of the fare must not match any of the fare
        /// basis codes received in the data file
        /// </summary>
        public string[] BrandExcludingFareBasisCode { get; set; }

        /// <summary>
        /// When a single fare type code is received in the ‘Brand Excluding FareTypeCode’ field, for the
        /// brand to match the fare, the fare type code for this fare must not match the fare type code
        /// received in the data file.
        /// When multiple fare type codes are received in the ‘Brand Excluding FareTypeCode’ field, for the
        /// brand to match the fare, the fare type code of the fare must not match any of the fare type
        /// codes received in the data file.
        /// </summary>
        public string[] BrandExcludingFareTypeCode { get; set; }

        /// <summary>
        /// When a single RBD is received in the ‘Brand Excluding RBD’ field, for the brand to match the
        /// fare, the RBD for this fare must not match the RBD received in the data file.
        /// When multiple RBD’s are received in the ‘Brand Excluding RBD’ field, for the brand to match the
        /// fare, the RBD of the fare must not match any of the RBDs received in the data file.
        /// </summary>
        public string[] BrandExcludingReservationBookingDesignor { get; set; }

        /// <summary>
        /// When a single tariff is received in the ‘Brand Excluding Tariff’ field, for the brand to match the
        /// fare, the tariff for this fare must not match the tariff received in the data file.
        /// When multiple tariffs are received in the ‘Brand Excluding Tariff’ field, for the brand to match
        /// the fare, the tariff of the fare must not match any of the tariffs received in the data file.
        /// </summary>
        public string[] BrandExcludingTariff { get; set; }

        /// <summary>
        /// When a single rule is received in the ‘Brand Excluding Rule’ field, for the brand to match the
        /// fare, the rule for this fare must not match the rule received in the data file.
        /// When multiple rules are received in the ‘Brand Excluding Rule’ field, for the brand to match the
        /// fare, the rule of the fare must not match any of the rules received in the data file.
        /// </summary>
        public string[] BrandExcludingRule { get; set; }


        /// <summary>
        /// Brand cabin is used to match brands only to specific cabins. You may receive one brand cabin in
        /// either the ‘Brand Cabin’ or ‘Brand CabinExcluding’ fields.
        /// When receiving a cabin code in this field, you will receive a one-letter identifier to confirm the
        /// cabin type being used:
        /// • Y = Economy
        /// • P = Premium Economy
        /// • C = Business
        /// • J = Premium Business
        /// • F = First Class
        /// • R = Premium First
        /// 
        /// When a brand cabin code is received in the ‘Brand Cabin’ field, for the brand to match the fare,
        /// the cabin specified in the file must be the cabin to which the fare is associated.
        /// Examples
        /// • Y = Economy must be the cabin associated with the fare
        /// • P = Premium Economy must be the cabin associated with the fare
        /// • C = Business must be the cabin associated with the fare
        /// • J = Premium Business must be the cabin associated with the fare
        /// • F = First Class must be the cabin associated with the fare
        /// • R = Premium First must be the cabin associated with the fare
        /// </summary>
        public string BrandCabin { get; set; }

        /// <summary>
        /// When a brand cabin code is received in the ‘Brand CabinExcluding’ field, for the brand to match
        /// the fare, the cabin specified in the file must not be the cabin the fare is associated to.
        /// Examples
        /// • Y = Economy must not be the cabin associated with the fare
        /// • P = Premium Economy must not be the cabin associated with the fare
        /// • C = Business must not be the cabin associated with the fare
        /// • J = Premium Business must not be the cabin associated with the fare
        /// • F = First Class must not be the cabin associated with the fare
        /// • R = Premium First must not be the cabin associated with the fare
        /// </summary>
        public string BrandCabinExcluding { get; set; }

        /// <summary>
        /// Brand equipment is used to match brands only to specific aircraft types. You may receive one
        /// brand equipment type in either the ‘Brand Equipment’ or ‘Brand EquipmentExcluding’ fields.
        /// 
        /// When an equipment code is received in the ‘Brand Equipment’ field, for the brand to match the
        /// fare, the equipment type specified in the file must match the aircraft type for the fare being
        /// checked
        /// </summary>
        public string BrandEquipment { get; set; }

        /// <summary>
        /// When an equipment code is received in the ‘Brand Equipment’ field, for the brand to match the
        /// fare, the equipment type specified in the file must match the aircraft type for the fare being
        /// checked
        /// </summary>
        public string BrandEquipmentExcluding { get; set; }

        public IEnumerable<FareRules> MatchingFareRules
        {
            get
            {
                string[] refData = BrandMatchingFareBasisCode;

                if (BrandMatchingFareTypeCode.Length > refData.Length)
                    refData = BrandMatchingFareTypeCode;
                if (BrandMatchingReservationBookingDesignor.Length > refData.Length)
                    refData = BrandMatchingReservationBookingDesignor;
                if (BrandMatchingTariff.Length > refData.Length)
                    refData = BrandMatchingTariff;
                if (BrandMatchingRule.Length > refData.Length)
                    refData = BrandMatchingRule;

                if (refData == null || !refData.Any())
                    return new List<FareRules>();

                return refData.Select((data, idx) => new FareRules
                {
                    FareBasisCode = GetArrayValue(BrandMatchingFareBasisCode, idx),
                    FareTypeCode = GetArrayValue(BrandMatchingFareTypeCode, idx),
                    ReservationBookingDesignor = GetArrayValue(BrandMatchingReservationBookingDesignor, idx),
                    Tariff = GetArrayValue(BrandMatchingTariff, idx),
                    Rule = GetArrayValue(BrandMatchingRule, idx),

                });
            }
        }

        public IEnumerable<FareRules> ExcludingFareRules
        {
            get
            {
                string[] refData = BrandExcludingFareBasisCode;

                if (BrandExcludingFareTypeCode.Length > refData.Length)
                    refData = BrandExcludingFareTypeCode;
                if (BrandExcludingReservationBookingDesignor.Length > refData.Length)
                    refData = BrandExcludingReservationBookingDesignor;
                if (BrandExcludingTariff.Length > refData.Length)
                    refData = BrandExcludingTariff;
                if (BrandExcludingRule.Length > refData.Length)
                    refData = BrandExcludingRule;

                if (refData == null || !refData.Any())
                    return new List<FareRules>();

                return refData.Select((data, idx) => new FareRules
                {
                    FareBasisCode = data,
                    FareTypeCode = GetArrayValue(BrandExcludingFareTypeCode, idx),
                    ReservationBookingDesignor = GetArrayValue(BrandExcludingReservationBookingDesignor, idx),
                    Tariff = GetArrayValue(BrandExcludingTariff, idx),
                    Rule = GetArrayValue(BrandExcludingRule, idx),

                });
            }
        }

        #endregion

        #region === Including/excluding/chargeable Service Data ===

        /// <summary>
        /// reference for each attribute that can be used to locate the matching
        /// attribute or segment branding on the relevant file.
        /// 
        /// By taking the list of Service IDs that are Included, Excluded, or Chargeable, you can reference
        /// these with the content of the Ancillary or Segment Branding file to find out more details on
        /// these services.
        /// </summary>
        public string[] BrandIncludedServiceID { get; set; }

        /// <summary>
        /// Airlines file attributes with various ‘Group’ and ‘Secondary Group’ and ‘Sub Group Codes’. The
        /// codes assigned to each ancillary can be found on the Ancillary file.
        /// As different airlines often use different combinations of these codes when offering the same
        /// type of attribute, to standardise these attributes, Travelport assign a ‘Tag’ to each of the 7
        /// mandatory ancillaries that are included with each brand.
        /// Using the ‘Tag’ to determine the ancillary type creates an easier way to identify the 7
        /// mandatory attributes.
        /// The following are the currently provided ‘Tags’ and their meanings:
        /// • Tag= "Checked Baggage" = Hold Baggage
        /// • Tag= "Carry On Hand Baggage" = Carry On Baggage
        /// • Tag= "Rebooking" = Changes
        /// • Tag= "Refund" = Refund Permitted
        /// • Tag= "Seat Assignment" = Pre - Reserved Seat Assignment
        /// • Tag= "Meals and Beverages” = Inflight Meal Service
        /// • Tag= "WiFi" = Onboard Internet
        /// When an attribute’s ‘Group’ and ‘Secondary Group’ and ‘Sub Group Codes’ combination
        /// determine it is not one of the attributes that have a ‘Tag’, there will be no ‘Tag’ provided.
        /// </summary>
        public string[] BrandIncludedTags { get; set; }

        /// <summary>
        /// The following information also applies to the Ancillary file’s Service Short Name (page 37).
        /// Travelport assign standardised coding in these fields where possible for Baggage that can be
        /// used to identify the amount and weight of carry on and hold bags this attribute is associated to.
        /// Where a bag is assigned as an attribute and contains this information, you will find a code like
        /// ‘Y,1,23, CY’ or ‘C,2,20, Bag’.
        /// The structure of these codes are as follows:
        /// • 1st position = cabin: Y= Economy Cabin / P= Premium / C = Business / F = First
        /// • 2nd position = number of bags
        /// • 3rd position = weight of bags in KG
        /// • 4th position = type of bag: Bag = Checked baggage / CY = Carry on baggage
        /// Checked baggage:
        /// • Economy Class 1 bag max 23kg = Y,1,23, Bag
        /// • Premium Economy Class 2 bags max 23kg each = P,2,23, Bag
        /// • Business Class 2 bags max 32kg each = C,2,32, Bag
        /// • First Class 3 bags max 32kg each = F,3,32, Bag
        /// Carry On baggage:
        /// • Economy Class 1 bag max 8kg = Y,1,8, CY
        /// • Premium Economy Class 1 bag max 12kg = P,1,12, CY
        /// • Business Class 2 bags max 8kg each = C,2,8, CY
        /// • First Class 3 bags max 8kg each = F,3,8, CY
        /// Where XX replaces the number or weight of bags, that element is not valid for that airline.Travelport
        /// Travelport Branded Fares Data Feed 32
        /// For example:
        /// Y, XX,40, Bag = Economy Class, Checked Bags max weight 40kg but with no max number of bags
        /// specified.
        /// </summary>
        public string[] BrandIncludedExternalShortName { get; set; }

        public string[] BrandExcludedServiceID { get; set; }

        public string[] BrandExcludedTags { get; set; }

        public string[] BrandExcludedExternalShortName { get; set; }

        public string[] BrandChargeableServiceID { get; set; }

        public string[] BrandChargeableTags { get; set; }

        public string[] BrandChargeableExternalShortName { get; set; }


        public IEnumerable<ServiceData> IncludedServiceData
        {
            get
            {
                string[] refData = BrandIncludedServiceID;

                if (BrandIncludedExternalShortName.Length > refData.Length)
                    refData = BrandIncludedExternalShortName;
                if (BrandIncludedTags.Length > refData.Length)
                    refData = BrandIncludedTags;
                
                if (refData == null || !refData.Any())
                    return new List<ServiceData>();

                return refData.Select((data, idx) => new ServiceData
                {
                    ServiceID = GetArrayValue(BrandIncludedServiceID, idx),
                    ExternalShortName = GetArrayValue(BrandIncludedExternalShortName, idx),
                    Tag = GetArrayValue(BrandIncludedTags, idx)
                });
            }
        }


        public IEnumerable<ServiceData> ExcludedServiceData
        {
            get
            {
                string[] refData = BrandExcludedServiceID;

                if (BrandExcludedExternalShortName.Length > refData.Length)
                    refData = BrandExcludedExternalShortName;
                if (BrandExcludedTags.Length > refData.Length)
                    refData = BrandExcludedTags;

                if (refData == null || !refData.Any())
                    return new List<ServiceData>();

                return refData.Select((data, idx) => new ServiceData
                {
                    ServiceID = GetArrayValue(BrandExcludedServiceID, idx),
                    ExternalShortName = GetArrayValue(BrandExcludedExternalShortName, idx),
                    Tag = GetArrayValue(BrandExcludedTags, idx)
                });
            }
        }

        public IEnumerable<ServiceData> ChargeableServiceData
        {
            get
            {
                string[] refData = BrandChargeableServiceID;

                if (BrandChargeableExternalShortName.Length > refData.Length)
                    refData = BrandChargeableExternalShortName;
                if (BrandChargeableTags.Length > refData.Length)
                    refData = BrandChargeableTags;

                if (refData == null || !refData.Any())
                    return new List<ServiceData>();

                return refData.Select((data, idx) => new ServiceData
                {
                    ServiceID = GetArrayValue(BrandChargeableServiceID, idx),
                    ExternalShortName = GetArrayValue(BrandChargeableExternalShortName, idx),
                    Tag = GetArrayValue(BrandChargeableTags, idx)
                });
            }
        }

        #endregion

    }
}