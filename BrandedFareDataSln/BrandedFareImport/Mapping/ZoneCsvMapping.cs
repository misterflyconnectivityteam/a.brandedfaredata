﻿using BrandedFareData.Entity;
using TinyCsvParser.Mapping;
using TinyCsvParser.TypeConverter;

namespace BrandedFareImport.Mapping
{
    public class ZoneCsvMapping : CsvMapping<Zone>
    {
        public ZoneCsvMapping(ITypeConverterProvider typeConverterProvider) : base(typeConverterProvider)
        {
            MapProperty(0, x => x.Number);
            MapProperty(1, x => x.Area);
            MapProperty(2, x => x.Description);
            MapProperty(3, x => x.Countries);
            MapProperty(4, x => x.CountriesAlpha3Code);
            MapProperty(5, x => x.CountriesAlpha2Code);
        }
    }
}