﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Linq;
using System.Reflection;

namespace BrandedFareWebApi.Swagger
{
    public class SwaggerExcludeFilter : IOperationFilter
    {

        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            throw new NotImplementedException();
        }


        public void Apply(Operation operation, OperationFilterContext context)
        {
            var apiDescription = context.ApiDescription;


            if (operation.Parameters == null)
            {
                return;
            }

            var lstParameter = operation.Parameters.ToList();

            foreach (var parameter in lstParameter)
            {
                if (!(parameter is NonBodyParameter))
                    continue;

                if (!parameter.In.Equals("query", StringComparison.Ordinal))
                    continue;

                var apiParameterDescription = context?.ApiDescription?.ParameterDescriptions?.SingleOrDefault(p =>
                                                               p.Name == parameter.Name
                                                          && p.ModelMetadata?.ContainerType != null
                                                          && p.ModelMetadata?.PropertyName != null);

                var metadata = apiParameterDescription?.ModelMetadata;
                var propertyInfo = metadata?.ContainerType?.GetTypeInfo()?.GetProperty(metadata.PropertyName);
                if (propertyInfo == null)
                    continue;

                var attributes = propertyInfo.GetCustomAttributes();
                foreach (var att in attributes)
                {

                    if (att.GetType() == typeof(SwaggerExcludeAttribute))
                    {
                        operation.Parameters.Remove(parameter);
                    }
                }
            }
        }
    }
}
