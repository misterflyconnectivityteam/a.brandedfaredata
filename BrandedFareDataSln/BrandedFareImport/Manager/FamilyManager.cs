﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandedFareData.Entity;
using BrandedFareImport.Mapping;
using TinyCsvParser;
using TinyCsvParser.Mapping;
using TinyCsvParser.Tokenizer.RFC4180;
using TinyCsvParser.TypeConverter;

namespace BrandedFareImport.Manager
{
    public class FamilyManager : IManager<Family>
    {
        private readonly string fileLocation;
        
        private readonly int degreeOfParallelism = 10;
        private readonly bool keepOrder = false;

        public string CsvConfigDisplay;
        private readonly ILanguageParse languageParse;

        public FamilyManager(string fileLocation, char separator, ILanguageParse languageParse)
        {

            // Use a " as Quote Character, a \\ as Escape Character and a , as Delimiter.
            var options = new Options('"', '\\', separator);

            // Initialize the Rfc4180 Tokenizer:
            var tokenizer = new RFC4180Tokenizer(options);

            CsvMapping = new FamilyCsvMapping(new StringArrayTypeConverterProvider());
            CsvParser = new CsvParser<Family>(new CsvParserOptions(true, null, tokenizer,  degreeOfParallelism, keepOrder), CsvMapping);

            CsvConfigDisplay = CsvParser.ToString();

            this.fileLocation = fileLocation;
            this.languageParse = languageParse;
        }

        public ICsvMapping<Family> CsvMapping { get; set; }
        public ICsvParser<Family> CsvParser { get; set; }

        public IEnumerable<Family> LoadFromFile()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(this.fileLocation))
                    return null;
                else if (!System.IO.File.Exists(this.fileLocation))
                    return null;
                else if (CsvParser == null)
                    return null;

                var result = (CsvParser as CsvParser<Family>)?.ReadFromFile(this.fileLocation, Encoding.UTF8);

                var languageCode = languageParse.ParseLanguageCode(this.fileLocation);

                if (result == null)
                    return null;
                else
                {
                    var families = result.Where(i => i.IsValid)
                          .Select(i => i.Result).ToList();

                    families.ForEach(f => f.LanguageCode = languageCode);

                    return families;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
