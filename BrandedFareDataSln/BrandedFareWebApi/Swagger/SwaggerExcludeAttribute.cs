﻿using System;

namespace BrandedFareWebApi.Swagger
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SwaggerExcludeAttribute : Attribute
    {

    }
}
