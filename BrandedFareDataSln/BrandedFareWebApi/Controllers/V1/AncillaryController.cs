﻿using BrandedFareData.Entity;
using BrandedFareWebApi.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;

namespace BrandedFareWebApi.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("/api/v{version:apiVersion}/[controller]")]
    public class AncillaryController : ControllerBase
    {

        private readonly string LogText = "[BrandedFareWebApi AncillaryController]";
        private readonly IBrandService brandService;

        public AncillaryController(IBrandService brandService)
        {
            this.brandService = brandService;
        }

        /// <summary>
        /// Get Brands
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>        
        [HttpGet("Ancillaries/{brandKey}", Name = "Ancillaries")]
        //[MapToApiVersion("1.0")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<Ancillary>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(IEnumerable<Ancillary>), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(IEnumerable<Ancillary>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(IEnumerable<Ancillary>), StatusCodes.Status500InternalServerError)]
        
        public ActionResult<IEnumerable<Ancillary>> Ancillaries(int brandKey)
        {
            //-->
            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine($"{LogText} GetCities() | START MAIN | TIME: {sw.Elapsed}");

            try
            {
                sw.Stop();
                //Console.WriteLine($"{LogText} GetCities() | END | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()}");

                if (brandKey == 0)
                    return null;

                return new OkObjectResult(brandService.FindAncillaries(brandKey));
                //return StatusCode(StatusCodes.Status200OK, data.Item2);
            }
            catch (Exception ex)
            {
                //Console.WriteLine($"{LogText} GetCities() | END-MAIN-ERROR | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()} | ERROR: {ex}");
                //return StatusCode(StatusCodes.Status500InternalServerError, new List<City>());
            }
            return null;
        }

        /// <summary>
        /// Get Brands
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>        
        [HttpGet("{ServiceID}", Name = "Ancillary")]
        //[MapToApiVersion("1.0")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(Ancillary), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Ancillary), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(Ancillary), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Ancillary), StatusCodes.Status500InternalServerError)]
        public ActionResult<Ancillary> Ancillary(int ServiceID)
        {
            //-->
            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine($"{LogText} GetCities() | START MAIN | TIME: {sw.Elapsed}");

            try
            {
                sw.Stop();
                //Console.WriteLine($"{LogText} GetCities() | END | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()}");

                if (ServiceID == 0)
                    return null;

                return new OkObjectResult(brandService.FindAncillary(ServiceID));
                //return StatusCode(StatusCodes.Status200OK, data.Item2);
            }
            catch (Exception ex)
            {
                //Console.WriteLine($"{LogText} GetCities() | END-MAIN-ERROR | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()} | ERROR: {ex}");
                //return StatusCode(StatusCodes.Status500InternalServerError, new List<City>());
            }
            return null;
        }


        /// <summary>
        /// Get Brands
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("IncludedAncillaries/{brandKey}", Name = "IncludedAncillaries")]
        //[MapToApiVersion("1.0")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<Ancillary>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(List<Ancillary>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(List<Ancillary>), StatusCodes.Status500InternalServerError)]
        public ActionResult<IEnumerable<Ancillary>> IncludedAncillaries(int brandKey)
        {
            //-->
            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine($"{LogText} GetCities() | START MAIN | TIME: {sw.Elapsed}");

            try
            {
                sw.Stop();
                //Console.WriteLine($"{LogText} GetCities() | END | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()}");

                if (brandKey == -1)
                    return null;

                return new OkObjectResult(brandService.FindIncludedAncillaries(brandKey));
                //return StatusCode(StatusCodes.Status200OK, data.Item2);
            }
            catch (Exception ex)
            {
                //Console.WriteLine($"{LogText} GetCities() | END-MAIN-ERROR | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()} | ERROR: {ex}");
                //return StatusCode(StatusCodes.Status500InternalServerError, new List<City>());
            }
            return null;
        }


        /// <summary>
        /// Get Brands
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("ChargeableAncillaries/{brandKey}", Name = "ChargeableAncillaries")]
        //[MapToApiVersion("1.0")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<Ancillary>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(List<Ancillary>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(List<Ancillary>), StatusCodes.Status500InternalServerError)]
        public ActionResult<IEnumerable<Ancillary>> ChargeableAncillaries(int brandKey)
        {
            //-->
            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine($"{LogText} GetCities() | START MAIN | TIME: {sw.Elapsed}");

            try
            {
                sw.Stop();
                //Console.WriteLine($"{LogText} GetCities() | END | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()}");

                if (brandKey == -1)
                    return null;

                return new OkObjectResult(brandService.FindChargeableAncillaries(brandKey));
                //return StatusCode(StatusCodes.Status200OK, data.Item2);
            }
            catch (Exception ex)
            {
                //Console.WriteLine($"{LogText} GetCities() | END-MAIN-ERROR | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()} | ERROR: {ex}");
                //return StatusCode(StatusCodes.Status500InternalServerError, new List<City>());
            }
            return null;
        }

        /// <summary>
        /// Get Brands
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("ExcludedAncillaries/{brandKey}", Name = "ExcludedAncillaries")]
        //[MapToApiVersion("1.0")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<Ancillary>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(List<Ancillary>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(List<Ancillary>), StatusCodes.Status500InternalServerError)]
        public ActionResult<IEnumerable<Ancillary>> ExcludedAncillaries(int brandKey)
        {
            //-->
            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine($"{LogText} GetCities() | START MAIN | TIME: {sw.Elapsed}");

            try
            {
                sw.Stop();
                //Console.WriteLine($"{LogText} GetCities() | END | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()}");

                if (brandKey == -1)
                    return null;

                return new OkObjectResult(brandService.FindExcludedAncillaries(brandKey));
                //return StatusCode(StatusCodes.Status200OK, data.Item2);
            }
            catch (Exception ex)
            {
                //Console.WriteLine($"{LogText} GetCities() | END-MAIN-ERROR | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()} | ERROR: {ex}");
                //return StatusCode(StatusCodes.Status500InternalServerError, new List<City>());
            }
            return null;
        }


    }
}