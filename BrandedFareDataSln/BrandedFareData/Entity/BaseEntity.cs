﻿using System.Linq;

namespace BrandedFareData.Entity
{
    public class BaseEntity
    {
        public string LanguageCode { get; set; }

        protected string GetArrayValue(string[] array, int idx)
        {
            if (array == null)
                return "";
            if (idx == -1)
                return "";
            return array.ElementAtOrDefault(idx) != null ? array?[idx] : "";
        }
    }
}
