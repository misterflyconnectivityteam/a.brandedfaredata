﻿using BrandedFareData.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace BrandedFareSearch.Search
{
    public interface ISearchFamily
    {
        IEnumerable<Family> SearchByCarrier(string carrier);

        IEnumerable<Family> SearchByDate(IEnumerable<Family> families, DateTime searchDate);

        IEnumerable<Family> SearchByTravelDate(IEnumerable<Family> families, Travel travel);
        IEnumerable<Family> SearchByPassengerTypeCode(IEnumerable<Family> families, IEnumerable<PTC> passengerTypeCodes);
        IEnumerable<Family> SearchByAccountCode(IEnumerable<Family> families, IEnumerable<string> accountCodes);

        IEnumerable<Family> SearchByFlightGeoGraphy(IEnumerable<Family> families, Travel travel);
    }
}
