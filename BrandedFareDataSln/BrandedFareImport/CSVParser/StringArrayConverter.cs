﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinyCsvParser.TypeConverter
{
    public class StringArrayConverter : BaseConverter<string[]>
    {
        public override bool TryConvert(string value, out string[] result)
        {
            result = value.Split(';').Select(t => t.Trim()).ToArray();

            return true;
        }
    }
}
