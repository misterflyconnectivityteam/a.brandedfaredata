﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrandedFareData.Entity
{
    public class Area
    {
        public string CodeIata { get; set; }
        public string Name { get; set; }

        public IEnumerable<Zone> Zones { get; set; }

    }
}
