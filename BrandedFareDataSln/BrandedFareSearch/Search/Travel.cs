﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrandedFareSearch.Search
{
    public class Travel
    {
        public string Carrier { get; set; }
        public IEnumerable<Itinerary> Itineraries { get; set; }

        public IEnumerable<Passenger> Passengers { get; set; }

        public IEnumerable<string> AccountCodes { get; set; }
    }

    public class Passenger
    {
        public PTC PTC { get; set; }
    }

    public enum PTC
    {
        ADT,
        CHD,
        INF
    }

    public class Itinerary
    {
        public DateTime DepartureDatetime { get; set; }
        public DateTime? ArrivalDatetime { get; set; }

        public string Origin { get; set; }
        public string Destination { get; set; }

        public string OriginCity { get; set; }
        public string DestinationCity { get; set; }

        public string MarketingCarrier { get; set; }

        public int Sequence { get; set; }

        public IEnumerable<Segment> Segments { get; set; }

    }

    public class Segment
    {
        public string Status { get; set; }
        public string Equipment { get; set; }
        public string StopReason { get; set; }
        public int Stops { get; set; }
        public int Miles { get; set; }
        public int FlightTime { get; set; }
        public string OperatingFlightNumber { get; set; }
        public string OperatingAirline { get; set; }
        public DateTime? ArrivalDatetime { get; set; }
        public string ArrivalTerminal { get; set; }
        public string ArrivalAirport { get; set; }
        public string ArrivalCity { get; set; }
        public DateTime DepartureDatetime { get; set; }
        public string DepartureTerminal { get; set; }
        public string DepartureAirport { get; set; }
        public string DepartureCity { get; set; }
        public string FlightNumber { get; set; }
        public string Airline { get; set; }
        public int Sequence { get; set; }        
        public Itinerary Itinerary { get; set; }
    }
}
