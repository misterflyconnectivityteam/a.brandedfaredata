﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandedFareData.Entity;
using BrandedFareImport.Mapping;
using TinyCsvParser;
using TinyCsvParser.Mapping;
using TinyCsvParser.Tokenizer;
using TinyCsvParser.Tokenizer.RFC4180;
using TinyCsvParser.TypeConverter;

namespace BrandedFareImport.Manager
{
    public class AncillaryManager : IManager<Ancillary>
    {
        private readonly string fileLocation;
        
        private readonly int degreeOfParallelism = 10;
        private readonly bool keepOrder = false;

        public string CsvConfigDisplay;
        private readonly ILanguageParse languageParse;

        public AncillaryManager(string fileLocation, char separator, ILanguageParse languageParse)
        {

            // Use a " as Quote Character, a \\ as Escape Character and a , as Delimiter.
            
            // Initialize the Rfc4180 Tokenizer:
            var tokenizer = new QuotedStringTokenizer('"', '\\', separator);

            CsvMapping = new AncillaryBrandCsvMapping(new StringArrayTypeConverterProvider());
            CsvParser = new CsvParser<Ancillary>(new CsvParserOptions(true, null, tokenizer,  degreeOfParallelism, keepOrder), CsvMapping);

            CsvConfigDisplay = CsvParser.ToString();

            this.fileLocation = fileLocation;
            this.languageParse = languageParse;
        }

        public ICsvMapping<Ancillary> CsvMapping { get; set; }
        public ICsvParser<Ancillary> CsvParser { get; set; }

        public IEnumerable<Ancillary> LoadFromFile()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(this.fileLocation))
                    return null;
                else if (!System.IO.File.Exists(this.fileLocation))
                    return null;
                else if (CsvParser == null)
                    return null;

                CsvReaderOptions csvReaderOptions = new CsvReaderOptions(new[] { "\r\n" });

                var contentFile = System.IO.File.ReadAllText(fileLocation);

                if (contentFile == null)
                    return null;

                //--> We don't use ReadFromFile because it does not handle the case newline inside "" : "    newline    "
                var result = (CsvParser as CsvParser<Ancillary>)?.ReadFromString(csvReaderOptions, contentFile);

                var languageCode = languageParse.ParseLanguageCode(this.fileLocation);

                if (result == null)
                    return null;
                else
                {
                    var ancillaries = result.Where(i => i.IsValid)
                          .Select(i => i.Result).ToList();

                    ancillaries.ForEach(f => f.LanguageCode = languageCode);

                    return ancillaries;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
