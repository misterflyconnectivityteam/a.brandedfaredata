﻿using BrandedFareData.Entity;
using Stateless;
using System;
using System.Collections.Generic;

namespace BrandedFareSearch.Search
{
    public class MatchFamily
    {
        #region === States && Triggers ===

        private enum State { Init, Carrier, Date, TravelDate, PassengerTypeCode, AccountCode, FlightGeography, End }
        private enum Trigger { SearchCarrier, SearchDate, SearchTravelDate, SearchPassengerTypeCode, SearchAccountTypeCodes, SearchFlightGeography, End, NotFound }

        #endregion

        #region === Triggers With Parameters ===

        private readonly StateMachine<State, Trigger>.TriggerWithParameters<string> _searchCarrierTrigger;
        private readonly StateMachine<State, Trigger>.TriggerWithParameters<DateTime> _searchDateTrigger;
        private readonly StateMachine<State, Trigger>.TriggerWithParameters<Travel> _searchTravelDateTrigger;
        private readonly StateMachine<State, Trigger>.TriggerWithParameters<IEnumerable<PTC>> _searchPassengerTypeCodeTrigger;
        private readonly StateMachine<State, Trigger>.TriggerWithParameters<Travel> _searchAccountTypeCodeTrigger;
        private readonly StateMachine<State, Trigger>.TriggerWithParameters<Travel> _searchFlightGeographyTrigger;
        #endregion

        #region === State Machine ===

        private readonly StateMachine<State, Trigger> _stateMachine;

        #endregion 

        public IEnumerable<Family> Families;
        private readonly ISearchFamily searchFamily;

        #region === Set Up the State Machine ===

        public MatchFamily(ISearchFamily searchFamily)
        {
            this.searchFamily = searchFamily;

            _stateMachine = new StateMachine<State, Trigger>(State.Init);

            _searchCarrierTrigger = _stateMachine.SetTriggerParameters<string>(Trigger.SearchCarrier);
            _searchDateTrigger = _stateMachine.SetTriggerParameters<DateTime>(Trigger.SearchDate);
            _searchTravelDateTrigger = _stateMachine.SetTriggerParameters<Travel>(Trigger.SearchTravelDate);
            _searchPassengerTypeCodeTrigger = _stateMachine.SetTriggerParameters<IEnumerable<PTC>>(Trigger.SearchPassengerTypeCode);
            _searchAccountTypeCodeTrigger = _stateMachine.SetTriggerParameters<Travel>(Trigger.SearchAccountTypeCodes);
            _searchFlightGeographyTrigger = _stateMachine.SetTriggerParameters<Travel>(Trigger.SearchFlightGeography);

            _stateMachine.Configure(State.Init)
                .Permit(Trigger.SearchCarrier, State.Carrier);

            _stateMachine.Configure(State.Carrier)
                .OnEntryFrom(_searchCarrierTrigger, OnSearchCarrier)
                .Permit(Trigger.SearchDate, State.Date);

            _stateMachine.Configure(State.Date)
                .OnEntryFrom(_searchDateTrigger, OnSearchDate)
             .Permit(Trigger.SearchTravelDate, State.TravelDate);

            _stateMachine.Configure(State.TravelDate)
                .OnEntryFrom(_searchTravelDateTrigger, OnSearchTravelDate)
             .Permit(Trigger.SearchPassengerTypeCode, State.PassengerTypeCode);

            _stateMachine.Configure(State.PassengerTypeCode)
               .OnEntryFrom(_searchPassengerTypeCodeTrigger, OnSearchPassengerTypeCode)
            .PermitIf(Trigger.SearchAccountTypeCodes, State.AccountCode);

            _stateMachine.Configure(State.AccountCode)
               .OnEntryFrom(_searchAccountTypeCodeTrigger, OnSearchAccountCodes)
            .PermitIf(Trigger.SearchFlightGeography, State.FlightGeography);

            _stateMachine.Configure(State.FlightGeography)
               .OnEntryFrom(_searchFlightGeographyTrigger, OnSearchFlightGeography)
            .PermitIf(Trigger.End, State.End);

            var graph = Stateless.Graph.UmlDotGraph.Format(_stateMachine.GetInfo());
        }
        #endregion

        #region === Search methods which trigger the transition of states ===

        public void SeachByCarrier(string carrier)
        {
            _stateMachine.Fire(_searchCarrierTrigger, carrier);
        }

        public void SearchBySearchDate(DateTime searchDate)
        {
            _stateMachine.Fire(_searchDateTrigger, searchDate);
        }

        public void SearchByTravelDates(Travel travel)
        {
            _stateMachine.Fire(_searchTravelDateTrigger, travel);
        }

        public void SearchByPassengerTypeCodes(IEnumerable<PTC> passengerTypeCodes)
        {
            _stateMachine.Fire(_searchPassengerTypeCodeTrigger, passengerTypeCodes);
        }

        public void SearchByAccountCodes(Travel travel)
        {
            _stateMachine.Fire(_searchAccountTypeCodeTrigger, travel);
        }

        public void SearchByFlightGeography(Travel travel)
        {
            _stateMachine.Fire(_searchFlightGeographyTrigger, travel);
        }

        #endregion

        #region === Event Methods which filter the families ===

        private void OnSearchAccountCodes(Travel travel)
        {
            if (travel == null)
                return;

            Families = searchFamily.SearchByAccountCode(Families, travel?.AccountCodes);
            
        }

        private void OnSearchPassengerTypeCode(IEnumerable<PTC> passengerTypeCodes)
        {
            if (passengerTypeCodes == null)
                return;

            Families = searchFamily.SearchByPassengerTypeCode(Families, passengerTypeCodes);
        }

        private void OnSearchTravelDate(Travel travel)
        {
            Families = searchFamily.SearchByTravelDate(Families, travel);
        }

        private void OnSearchDate(DateTime searchDate)
        {
            Families = searchFamily.SearchByDate(Families, searchDate);
        }

        private void OnSearchCarrier(string carrier)
        {
            Families = searchFamily.SearchByCarrier(carrier);
        }

        private void OnSearchFlightGeography(Travel travel)
        {
            Families = searchFamily.SearchByFlightGeoGraphy(Families, travel);

            _stateMachine.Fire(Trigger.End);
        }

        #endregion

    }
}