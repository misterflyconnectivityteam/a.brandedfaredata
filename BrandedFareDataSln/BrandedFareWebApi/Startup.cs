using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BrandedFareData.Entity;
using BrandedFareImport.Manager;
using BrandedFareWebApi.Services;
using BrandedFareWebApi.Services.Interfaces;
using BrandedFareWebApi.Swagger;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace BrandedFareWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            #region === Swagger ===
            services.AddApiVersioning(
                options =>
                {
                    // reporting api versions will return the headers "api-supported-versions" and "api-deprecated-versions"
                    options.ReportApiVersions = true;
                });
            services.AddVersionedApiExplorer(
                options =>
                {
                    // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
                    // note: the specified format code will format the version as "'v'major[.minor][-status]"
                    options.GroupNameFormat = "'v'VVV";

                    // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                    // can also be used to control the format of the API version in route templates
                    options.SubstituteApiVersionInUrl = true;
                });
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
            services.AddSwaggerGen(
                options =>
                {
                    // add a custom operation filter which sets default values
                    options.OperationFilter<SwaggerDefaultValues>();

                    options.OperationFilter<SwaggerExcludeFilter>();

                    // integrate xml comments
                    List<string> xmlFiles = Directory.GetFiles(AppContext.BaseDirectory, "*.xml", SearchOption.TopDirectoryOnly).ToList();

                    xmlFiles.ForEach(xmlFile => options.IncludeXmlComments(xmlFile));
                });

            
            services.AddSingleton<IBrandService, BrandService>();            
            services.AddSingleton<IManager<Brand>>(x => new BrandManager("Data/Brand.csv", ',', new LanguageParse()));
            services.AddSingleton<IManager<Family>>(x => new FamilyManager("Data/Family.csv", ',', new LanguageParse()));
            services.AddSingleton<IManager<Ancillary>>(x => new AncillaryManager("Data/Ancillary.csv", ',', new LanguageParse()));
            services.AddSingleton<IManager<Zone>>(x => new ZoneManager("Data/Zone.csv", ','));            

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApiVersionDescriptionProvider provider)
        {
              
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for 
                // production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseMvc();

            #region === Swagger ===
            //var option = new RewriteOptions();
            //option.AddRedirect("^$", "swagger");
            //app.UseRewriter(option);

            app.UseSwagger();
            app.UseSwaggerUI(
                options =>
                {
                    // build a swagger endpoint for each discovered API version
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
                        // to make Swagger UI available at the app's root
                        options.RoutePrefix = string.Empty;
                    }
                });
            #endregion

            var sw = System.Diagnostics.Stopwatch.StartNew();
            Console.WriteLine($"PreLoad Data Start");

            //--> Preload
            app.ApplicationServices.GetService<IBrandService>();
            sw.Stop();

            Console.WriteLine($"PreLoad Data End TIME: {sw.Elapsed}");

        }
    }
}
