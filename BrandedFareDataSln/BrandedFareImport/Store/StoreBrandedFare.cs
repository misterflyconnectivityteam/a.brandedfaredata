﻿using BrandedFareData.Entity;
using StackExchange.Redis;
using StackExchange.Redis.Extensions;
using StackExchange.Redis.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandedFareImport.Store
{
    public class RedisStoreBrandedFare : IStoreBrandedFare
    {
        public ICacheClient Client { get; set; }
        public RedisStoreBrandedFare(ICacheClient client)
        {
            Client = client;
        }

        public void InsertFamilies(IEnumerable<Family> families)
        {
            List<Task> addTasks = new List<Task>();
            foreach (var item in families)
            {
                Task<bool> addAsync = Client.AddAsync<Family>(item.ID.ToString(), item);
                addTasks.Add(addAsync);
            }
            Task[] tasks = addTasks.ToArray();
            Task.WaitAll(tasks);
        }
    }
}
