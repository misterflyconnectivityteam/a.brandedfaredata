﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrandedFareData.Entity
{
    public class Country
    {
        public string CountriesAlpha3Code { get; set; }
        public string CountriesAlpha2Code { get; set; }
        public string Name { get; set; }


    }
}
