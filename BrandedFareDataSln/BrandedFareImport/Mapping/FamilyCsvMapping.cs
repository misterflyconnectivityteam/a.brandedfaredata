﻿using BrandedFareData.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using TinyCsvParser.Mapping;
using TinyCsvParser.TypeConverter;

namespace BrandedFareImport.Mapping
{
    public class FamilyCsvMapping : CsvMapping<Family>
    {
        public FamilyCsvMapping(ITypeConverterProvider typeConverterProvider) : base(typeConverterProvider)
        {
            MapProperty(0, x => x.ID);
            MapProperty(1, x => x.CarrierCode);
            MapProperty(2, x => x.FareFamilyName);
            MapProperty(3, x => x.FareFamilyNameSearch);
            MapProperty(4, x => x.NewFamilyIndicator);
            MapProperty(5, x => x.PassengerTypeCode);
            MapProperty(6, x => x.EffectiveSearchDate, new DateTimeConverter("dd/MM/yyyy"));
            MapProperty(7, x => x.DiscontinueSearchDate, new DateTimeConverter("dd/MM/yyyy"));
            MapProperty(8, x => x.PublicIndicator);
            MapProperty(9, x => x.EffectiveTravelDate, new DateTimeConverter("dd/MM/yyyy"));
            MapProperty(10, x => x.DiscontinueTravelDate, new DateTimeConverter("dd/MM/yyyy"));
            MapProperty(11, x => x.SequenceNumber);
            MapProperty(12, x => x.ATPCOProgramCode);
            MapProperty(13, x => x.ATPCOProgramDescription);
            MapProperty(14, x => x.ATPCOSequenceNumber);
            MapProperty(15, x => x.VersionNumber);
            MapProperty(16, x => x.SourceType);
            MapProperty(17, x => x.Status);
            MapProperty(18, x => x.AccountTypeCode);            
            MapProperty(19,  x => x.IATA);
            MapProperty(20, x => x.GDS);
            MapProperty(21, x => x.PCC);
            MapProperty(22, x => x.SecurityLocationTypeCode);
            MapProperty(23, x => x.SecurityLocationType);
            MapProperty(24, x => x.PermittedInd);
            MapProperty(25, x => x.LocationOneTypeCode);
            MapProperty(26, x => x.LocationOne);
            MapProperty(27, x => x.LocationTwoTypeCode);
            MapProperty(28, x => x.LocationTwo);
            MapProperty(29, x => x.PermittedIndicator);
            MapProperty(30, x => x.DirectionApplicationCode);
            MapProperty(31, x => x.GlobalIndicator);
        }
    }
}
