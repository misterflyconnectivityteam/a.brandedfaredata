﻿using BrandedFareData.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace BrandedFareSearch.Search
{
    public interface IFlightGeoGraphy
    {
        IEnumerable<Family> FilterNoPermittedAirportsRules(IEnumerable<Family> families, Travel travel);
        IEnumerable<Family> FilterNoPermittedCitiesRules(IEnumerable<Family> families, Travel travel);
        //IEnumerable<Family> FilterPermittedAirportsRules(IEnumerable<Family> families, Travel travel);

        //IEnumerable<Family> FilterNoPermittedCitiesRules(IEnumerable<Family> families, Travel travel);
        //IEnumerable<Family> FilterPermittedCitiesRules(IEnumerable<Family> families, Travel travel);
    }
}
