﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrandedFareData.Entity
{
    public class FlightGeographyRules
    {
        public int ID { get; set; }
        public string LocationOneTypeCode { get; set; }

        public string LocationOne { get; set; }

        public string LocationTwoTypeCode { get; set; }

        public string LocationTwo { get; set; }

        public string PermittedIndicator { get; set; }

        public string DirectionApplicationCode { get; set; }

    }
}
