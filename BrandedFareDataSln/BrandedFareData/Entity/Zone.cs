﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandedFareData.Entity
{
    public class Zone : BaseEntity
    {
        public string Number { get; set; }
        public string Area { get; set; }
        public string Description { get; set; }
        public string[] Countries { get; set; }
        public string[] CountriesAlpha3Code { get; set; }
        public string[] CountriesAlpha2Code { get; set; }


        public IEnumerable<Country> Country
        {
            get
            {
                string[] refData = Countries;

                if (CountriesAlpha3Code.Length > refData.Length)
                    refData = CountriesAlpha3Code;
                if (CountriesAlpha2Code.Length > refData.Length)
                    refData = CountriesAlpha2Code;

                if (refData == null || !refData.Any())
                    return new List<Country>();

                return refData.Select((data, idx) => new Country
                {
                    CountriesAlpha3Code = GetArrayValue(CountriesAlpha3Code, idx),
                    CountriesAlpha2Code = GetArrayValue(CountriesAlpha2Code, idx),
                    Name = GetArrayValue(Countries, idx)
                });
            }
        }
    }
}
