﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrandedFareData.Entity
{
    public class PointOfSaleRules
    {
        public int ID { get; set; }
        public string IATA { get; set; }

        public string GDS { get; set; }

        public string PCC { get; set; }

        public string SecurityLocationTypeCode { get; set; }

        public string SecurityLocationType { get; set; }

        public string PermittedInd { get; set; }

    }
}
