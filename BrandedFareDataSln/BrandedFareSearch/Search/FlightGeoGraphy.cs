﻿using BrandedFareData.Entity;
using System.Collections.Generic;
using System.Linq;

namespace BrandedFareSearch.Search
{
    public class FlightGeoGraphy : IFlightGeoGraphy
    {
        public IEnumerable<Family> FilterNoPermittedAirportsRules(IEnumerable<Family> families, Travel travel)
        {
            if (families == null)
                return families;

            if (travel?.Itineraries == null || travel.Itineraries.All(i => i.Segments == null))
                return families;

            var segments = travel.Itineraries.SelectMany(i => i.Segments);

            #region === Filter By AirPorts ===

            IEnumerable<Family> result = families.Where(f =>
                                            {
                                                //--> Get Rules No Permitted of type Airport with no DirectionApplicationCode
                                                var rules = f.FlightGeographyRules.Where(rule =>

                                                //--> Rule not permitted, if true the family must be removed
                                                rule.PermittedIndicator == "N"

                                                //--> Type "P" : AirPort
                                                && rule.LocationOneTypeCode == "P"
                                                && rule.LocationTwoTypeCode == "P"

                                                //-->
                                                && (!string.IsNullOrEmpty(rule.LocationOne)
                                                    && !string.IsNullOrEmpty(rule.LocationTwo))

                                                &&
                                                //--> If Empty, means the fare routing can travel in any direction between 
                                                //--> the geography supplied in ‘Location One’ and the geography supplied in ‘Location Two’
                                                string.IsNullOrEmpty(rule.DirectionApplicationCode));

                                                //--> No rules we return the family
                                                if (rules == null || !rules.Any())
                                                    return true;

                                                //--> Rules, we check the family don't match any, if it doesnt match, we return the family
                                                return !rules.All(
                                                    rule =>

                                                    (segments.Any(s => s.DepartureAirport == rule.LocationOne
                                                                     && s.ArrivalAirport == rule.LocationTwo) ||
                                                    segments.Any(s => s.DepartureAirport == rule.LocationTwo
                                                                     && s.ArrivalAirport == rule.LocationOne)));
                                            });

            result = result.Where(f =>
                        {
                            //--> Get Rules No Permitted of type Airport with no DirectionApplicationCode
                            var rules = f.FlightGeographyRules.Where(rule =>

                            //--> Rule not permitted, if true the family must be removed
                            rule.PermittedIndicator == "N"

                            //--> Type "P" : AirPort
                            && rule.LocationOneTypeCode == "P"
                            && rule.LocationTwoTypeCode == "P"

                            //-->
                            && (!string.IsNullOrEmpty(rule.LocationOne)
                                && !string.IsNullOrEmpty(rule.LocationTwo))

                            &&
                            //--> Receiving a ‘1’ or ‘01’ as the ‘Direction Application Code’ means the fare routing must travel
                            //--> from the geography supplied in ‘Location One’ to the geography supplied in ‘Location Two’.
                             (rule.DirectionApplicationCode == "1" || rule.DirectionApplicationCode == "01"));

                            //--> No rules we return the family
                            if (rules == null || !rules.Any())
                                return true;

                            //--> Rules, we check the family don't match any, if it doesnt match, we return the family
                            return !rules.All(
                                rule =>

                              segments.Any(s => s.DepartureAirport == rule.LocationOne && s.ArrivalAirport == rule.LocationTwo));
                        });

            result = result.Where(f =>
                        {
                            //--> Get Rules No Permitted of type Airport with no DirectionApplicationCode
                            var rules = f.FlightGeographyRules.Where(rule =>

                            //--> Rule not permitted, if true the family must be removed
                            rule.PermittedIndicator == "N"

                            //--> Type "P" : AirPort
                            && rule.LocationOneTypeCode == "P"                            

                            //-->
                            && (!string.IsNullOrEmpty(rule.LocationOne))

                            &&
                             //--> Receiving a ‘2’ or ‘02’ as the ‘Direction Application Code’ means the fare routing must travel
                             //--> from the geography supplied in ‘Location One’.
                             //--> When you receive a ‘2’ or ‘02’ there will be no data received in the ‘Location Two’ field.
                             (rule.DirectionApplicationCode == "2" || rule.DirectionApplicationCode == "02"));

                            //--> No rules we return the family
                            if (rules == null || !rules.Any())
                                return true;

                            //--> Rules, we check the family don't match any, if it doesnt match, we return the family
                            return !rules.All(
                                rule =>

                               segments.Any(s => s.DepartureAirport == rule.LocationOne));
                        });





            #endregion

            return result.ToList();
        }

        public IEnumerable<Family> FilterNoPermittedCitiesRules(IEnumerable<Family> families, Travel travel)
        {

            if (families == null)
                return families;

            if (travel?.Itineraries == null || travel.Itineraries.All(i => i.Segments == null))
                return families;

            var segments = travel.Itineraries.SelectMany(i => i.Segments);

            #region === Filter By AirPorts ===

            IEnumerable<Family> result = families.Where(f =>
            {
                //--> Get Rules No Permitted of type Airport with no DirectionApplicationCode
                var rules = f.FlightGeographyRules.Where(rule =>

                //--> Rule not permitted, if true the family must be removed
                rule.PermittedIndicator == "N"

                //--> Type "P" : AirPort
                && rule.LocationOneTypeCode == "C"
                && rule.LocationTwoTypeCode == "C"

                //-->
                && (!string.IsNullOrEmpty(rule.LocationOne)
                    && !string.IsNullOrEmpty(rule.LocationTwo))

                &&
                //--> If Empty, means the fare routing can travel in any direction between 
                //--> the geography supplied in ‘Location One’ and the geography supplied in ‘Location Two’
                string.IsNullOrEmpty(rule.DirectionApplicationCode));

                //--> No rules we return the family
                if (rules == null || !rules.Any())
                    return true;

                //--> Rules, we check the family don't match any, if it doesnt match, we return the family
                return !rules.All(
                    rule =>

                    (segments.Any(s => s.DepartureAirport == rule.LocationOne
                                     && s.ArrivalAirport == rule.LocationTwo) ||
                    segments.Any(s => s.DepartureAirport == rule.LocationTwo
                                     && s.ArrivalAirport == rule.LocationOne)));
            });

            result = result.Where(f =>
            {
                //--> Get Rules No Permitted of type Airport with no DirectionApplicationCode
                var rules = f.FlightGeographyRules.Where(rule =>

                //--> Rule not permitted, if true the family must be removed
                rule.PermittedIndicator == "N"

                //--> Type "P" : AirPort
                && rule.LocationOneTypeCode == "C"
                && rule.LocationTwoTypeCode == "C"

                //-->
                && (!string.IsNullOrEmpty(rule.LocationOne)
                    && !string.IsNullOrEmpty(rule.LocationTwo))

                &&
                 //--> Receiving a ‘1’ or ‘01’ as the ‘Direction Application Code’ means the fare routing must travel
                 //--> from the geography supplied in ‘Location One’ to the geography supplied in ‘Location Two’.
                 (rule.DirectionApplicationCode == "1" || rule.DirectionApplicationCode == "01"));

                //--> No rules we return the family
                if (rules == null || !rules.Any())
                    return true;

                //--> Rules, we check the family don't match any, if it doesnt match, we return the family
                return !rules.All(
                    rule =>

                  segments.Any(s => s.DepartureAirport == rule.LocationOne && s.ArrivalAirport == rule.LocationTwo));
            });

            result = result.Where(f =>
            {
                //--> Get Rules No Permitted of type Airport with no DirectionApplicationCode
                var rules = f.FlightGeographyRules.Where(rule =>

                //--> Rule not permitted, if true the family must be removed
                rule.PermittedIndicator == "N"

                //--> Type "P" : AirPort
                && rule.LocationOneTypeCode == "C"

                //-->
                && (!string.IsNullOrEmpty(rule.LocationOne))

                &&
                 //--> Receiving a ‘2’ or ‘02’ as the ‘Direction Application Code’ means the fare routing must travel
                 //--> from the geography supplied in ‘Location One’.
                 //--> When you receive a ‘2’ or ‘02’ there will be no data received in the ‘Location Two’ field.
                 (rule.DirectionApplicationCode == "2" || rule.DirectionApplicationCode == "02"));

                //--> No rules we return the family
                if (rules == null || !rules.Any())
                    return true;

                //--> Rules, we check the family don't match any, if it doesnt match, we return the family
                return !rules.All(
                    rule =>

                   segments.Any(s => s.DepartureAirport == rule.LocationOne));
            });


            #endregion

            return result.ToList();
        }
    }
}