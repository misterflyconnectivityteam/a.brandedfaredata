﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrandedFareData.Entity
{
    public class ServiceData
    {
        public string ServiceID { get; set; }

        public string Tag { get; set; }

        public string ExternalShortName { get; set; }
    }
}
