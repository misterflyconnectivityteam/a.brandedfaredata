﻿using BrandedFareData.Entity;
using BrandedFareSearch.Search;
using BrandedFareWebApi.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;

namespace BrandedFareWebApi.Controllers
{
    [ApiController]
    [Produces("application/json")]
    //[ApiVersion("1.0")]
    [Route("/api/v{version:apiVersion}/[controller]")]
    public class FamilyController : ControllerBase
    {

        private readonly string LogText = "[BrandedFareWebApi FamilyController]";
        private readonly IBrandService brandService;

        public FamilyController(IBrandService brandService, ILogger<FamilyController> logger)
        {
            this.brandService = brandService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("Families")]
        //[MapToApiVersion("1.0")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<Family>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(List<Family>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(List<Family>), StatusCodes.Status500InternalServerError)]
        public ActionResult<IEnumerable<Family>> Families()
        {
            //-->
            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine($"{LogText} GetFamilies() | START MAIN | TIME: {sw.Elapsed}");

            try
            {
                sw.Stop();
                //Console.WriteLine($"{LogText} GetCities() | END | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()}");

                return new OkObjectResult(brandService.Get());
                //return StatusCode(StatusCodes.Status200OK, data.Item2);
            }
            catch (Exception ex)
            {
                //Console.WriteLine($"{LogText} GetCities() | END-MAIN-ERROR | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()} | ERROR: {ex}");
                //return StatusCode(StatusCodes.Status500InternalServerError, new List<City>());
            }
            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="carrierCode"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        [HttpPost()]
        //[MapToApiVersion("1.0")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(Family), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Family), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Family), StatusCodes.Status500InternalServerError)]
        public ActionResult<Family> Family([FromBody]Travel travel, string provider)
        {
            //-->
            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine($"{LogText} GetFamilies(string carrierCode) | START MAIN | TIME: {sw.Elapsed}");

            try
            {
                sw.Stop();
                //Console.WriteLine($"{LogText} GetCities() | END | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()}");

                return new OkObjectResult(brandService.Find(provider, travel));
                //return StatusCode(StatusCodes.Status200OK, data.Item2);
            }
            catch (Exception ex)
            {
                //Console.WriteLine($"{LogText} GetCities() | END-MAIN-ERROR | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()} | ERROR: {ex}");
                //return StatusCode(StatusCodes.Status500InternalServerError, new List<City>());
            }
            return null;
        }


    }
}