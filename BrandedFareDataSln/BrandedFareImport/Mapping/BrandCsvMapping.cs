﻿using BrandedFareData.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using TinyCsvParser.Mapping;
using TinyCsvParser.TypeConverter;

namespace BrandedFareImport.Mapping
{
    public class BrandCsvMapping : CsvMapping<Brand>
    {
        public BrandCsvMapping(ITypeConverterProvider typeConverterProvider) : base(typeConverterProvider)
        {
            MapProperty(0, x => x.FareFamilyID);
            MapProperty(1, x => x.BrandKey);
            MapProperty(2, x => x.CarrierCode);
            MapProperty(3, x => x.BrandNameTitle);
            MapProperty(4, x => x.BrandExternalName);
            MapProperty(5, x => x.BrandShortName);
            MapProperty(6, x => x.BrandStrapLine);
            MapProperty(7, x => x.BrandSequenceNumber);
            MapProperty(8, x => x.BrandUpsellImageLocation);
            MapProperty(9, x => x.BrandConsumerImageLocation);
            MapProperty(10, x => x.BrandCode);
            MapProperty(11, x => x.BrandCodeExcluding);
            //--> no same order
            MapProperty(12, x => x.BrandMatchingFlightRanges);
            MapProperty(13, x => x.BrandMatchingFlightNumbers);
            //
            MapProperty(14, x => x.BrandMatchingFareBasisCode);
            MapProperty(15, x => x.BrandMatchingFareTypeCode);
            MapProperty(16, x => x.BrandMatchingReservationBookingDesignor);
            MapProperty(17, x => x.BrandMatchingTariff);
            MapProperty(18, x => x.BrandMatchingRule);
            //--> no same order
            MapProperty(19, x => x.BrandExcludingFlightNumbers);
            MapProperty(20, x => x.BrandExcludingFlightRanges);
            //
            MapProperty(21, x => x.BrandExcludingFareBasisCode);
            MapProperty(22, x => x.BrandExcludingFareTypeCode);
            MapProperty(23, x => x.BrandExcludingReservationBookingDesignor);
            MapProperty(24, x => x.BrandExcludingTariff);
            MapProperty(25, x => x.BrandExcludingRule);
            MapProperty(26, x => x.BrandUpsellText);
            MapProperty(27, x => x.BrandConsumerText);
            MapProperty(28, x => x.BrandCabin);
            MapProperty(29, x => x.BrandCabinExcluding);
            MapProperty(30, x => x.BrandEquipment);
            MapProperty(31, x => x.BrandEquipmentExcluding);            
            MapProperty(32, x => x.BrandIncludedServiceID);
            MapProperty(33, x => x.BrandIncludedTags);
            MapProperty(34, x => x.BrandIncludedExternalShortName);
            MapProperty(35, x => x.BrandExcludedServiceID);
            MapProperty(36, x => x.BrandExcludedTags);
            MapProperty(37, x => x.BrandExcludedExternalShortName);
            MapProperty(38, x => x.BrandChargeableServiceID);
            MapProperty(39, x => x.BrandChargeableTags);
            MapProperty(40, x => x.BrandChargeableExternalShortName);            
            MapProperty(41, x => x.BrandRuleText);
        }
    }
}
