﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrandedFareImport.Manager
{
    public interface ILanguageParse
    {
        string ParseLanguageCode(string fileLocation);
    }
}
