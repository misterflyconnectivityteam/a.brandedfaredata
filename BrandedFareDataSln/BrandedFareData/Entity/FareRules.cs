﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrandedFareData.Entity
{
    public class FareRules
    {
        public int ID { get; set; }
        public string FareBasisCode { get; set; }

        public string FareTypeCode { get; set; }

        public string ReservationBookingDesignor { get; set; }

        public string Tariff { get; set; }

        public string Rule { get; set; }

    }
}
