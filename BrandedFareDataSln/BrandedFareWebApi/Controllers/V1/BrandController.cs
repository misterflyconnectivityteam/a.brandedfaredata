﻿using BrandedFareData.Entity;
using BrandedFareWebApi.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;

namespace BrandedFareWebApi.Controllers
{
    [ApiController]
    [Produces("application/json")]
    //[ApiVersion("1.0")]
    [Route("/api/v{version:apiVersion}/[controller]")]
    public class BrandController : ControllerBase
    {

        private readonly string LogText = "[BrandedFareWebApi BrandController]";
        private readonly IBrandService brandService;

        public BrandController(IBrandService brandService)
        {
            this.brandService = brandService;
        }
       
        /// <summary>
        /// Get Brands
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>        
        [HttpGet("Brands/{familyId}", Name = "Brands")]
        //[MapToApiVersion("1.0")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<Brand>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(List<Brand>), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(List<Brand>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(List<Brand>), StatusCodes.Status500InternalServerError)]
        public ActionResult<IEnumerable<Brand>> Brands(int familyId)
        {
            //-->
            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine($"{LogText} GetCities() | START MAIN | TIME: {sw.Elapsed}");

            try
            {
                sw.Stop();
                //Console.WriteLine($"{LogText} GetCities() | END | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()}");

                if (familyId == 0)
                    return null;

                return new OkObjectResult(brandService.Get(familyId));
                //return StatusCode(StatusCodes.Status200OK, data.Item2);
            }
            catch (Exception ex)
            {
                //Console.WriteLine($"{LogText} GetCities() | END-MAIN-ERROR | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()} | ERROR: {ex}");
                //return StatusCode(StatusCodes.Status500InternalServerError, new List<City>());
            }
            return null;
        }

        /// <summary>
        /// Get Brands
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("{brandKey}", Name = "Brand")]
        //[MapToApiVersion("1.0")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(Brand), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Brand), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Brand), StatusCodes.Status500InternalServerError)]
        public ActionResult<Brand> Brand(int brandKey)
        {
            //-->
            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine($"{LogText} Brand() | START MAIN | TIME: {sw.Elapsed} Brand Key {brandKey}");

            try
            {
                sw.Stop();
                //Console.WriteLine($"{LogText} GetCities() | END | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()}");

                if (brandKey == -1)
                    return null;

                var brand = brandService.Find(brandKey);

                Console.WriteLine($"Brand Found ? {brand != null}");

                return new OkObjectResult(brand);
                //return StatusCode(StatusCodes.Status200OK, data.Item2);
            }
            catch (Exception ex)
            {
                //Console.WriteLine($"{LogText} GetCities() | END-MAIN-ERROR | TIME: {sw.Elapsed} | StatusCode: {requestStatus.ToString()} | ERROR: {ex}");
                //return StatusCode(StatusCodes.Status500InternalServerError, new List<City>());
            }
            return null;
        }

       
    }
}