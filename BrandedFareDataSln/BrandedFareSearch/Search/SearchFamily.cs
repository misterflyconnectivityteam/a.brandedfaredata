﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandedFareData.Entity;

namespace BrandedFareSearch.Search
{
    public class SearchFamily : ISearchFamily
    {
        private readonly IEnumerable<Family> families;
        private readonly IFlightGeoGraphy flightGeoGraphy;

        public SearchFamily(IEnumerable<Family> families)
        {
            this.families = families;
            this.flightGeoGraphy = new FlightGeoGraphy();

        }

        public IEnumerable<Family> SearchByDate(IEnumerable<Family> families, DateTime searchDate)
        {
            return families.Where(f => f.EffectiveSearchDate <= searchDate
                                            && searchDate <= f.DiscontinueSearchDate);
        }

        public IEnumerable<Family> SearchByCarrier(string carrier)
        {
            return this.families.Where(f => f.CarrierCode.ToUpperInvariant() == carrier.Trim().ToUpperInvariant());
        }

        public IEnumerable<Family> SearchByTravelDate(IEnumerable<Family> families, Travel travel)
        {

            var departureDateTimes = travel?.Itineraries?.Where(i => i.Segments != null).SelectMany(i => i.Segments.Select(s => s.DepartureDatetime));
            var arrivalDateTimes = travel?.Itineraries?.Where(i => i.Segments != null).SelectMany(i => i.Segments.Select(s => s.ArrivalDatetime));

            if (departureDateTimes == null || !departureDateTimes.Any())
            {
                departureDateTimes = travel?.Itineraries?.Select(i => i.DepartureDatetime);
                arrivalDateTimes = travel?.Itineraries?.Select(i => i.ArrivalDatetime);
            }


            return families.Where(f =>
                 (departureDateTimes == null || !departureDateTimes.Any()) ? true : departureDateTimes.All(t => f.EffectiveTravelDate <= t && t <= f.DiscontinueTravelDate)
                  &&
                 (arrivalDateTimes == null || !arrivalDateTimes.Any() || arrivalDateTimes.All(ar => !ar.HasValue)) ? true :
                 arrivalDateTimes.All(t => f.EffectiveTravelDate <= t && t <= f.DiscontinueTravelDate)
            );
        }

        public IEnumerable<Family> SearchByPassengerTypeCode(IEnumerable<Family> families, IEnumerable<PTC> passengerTypeCodes)
        {
            if (passengerTypeCodes == null)
                return families;

            if (families.All(f => string.IsNullOrWhiteSpace(f.PassengerTypeCode)))
                return families;

            return families.Where(f =>
            {

                if (string.IsNullOrWhiteSpace(f.PassengerTypeCode))
                    return true;
                if (!passengerTypeCodes.Any(p => p.ToString().ToUpperInvariant() == f.PassengerTypeCode.ToUpperInvariant()))
                    return false;
                else
                    return true;
            });
        }

        public IEnumerable<Family> SearchByAccountCode(IEnumerable<Family> families, IEnumerable<string> accountCodes)
        {
            if (accountCodes == null)
                return families;

            if (families.All(f => string.IsNullOrWhiteSpace(f.AccountTypeCode)))
                return families;

            return families.Where(f =>
            {

                if (string.IsNullOrWhiteSpace(f.PassengerTypeCode))
                    return true;
                if (!accountCodes.Any(p => p == f.AccountTypeCode))
                    return false;
                else
                    return true;
            });
        }

        public IEnumerable<Family> SearchByFlightGeoGraphy(IEnumerable<Family> families, Travel travel)
        {
            if (families.All(f => f.FlightGeographyRules == null || !f.FlightGeographyRules.Any()))
                return families;

            //--> Remove all families which match No Permitted Rules
            var result = flightGeoGraphy.FilterNoPermittedAirportsRules(families, travel);

            result = flightGeoGraphy.FilterNoPermittedCitiesRules(result, travel);



            //result = flightGeoGraphy.FilterPermittedAirportsRules(result, travel);

            //result = flightGeoGraphy.FilterNoPermittedCitiesRules(result, travel);

            //result = flightGeoGraphy.FilterPermittedCitiesRules(result, travel);

            return result;
        }
    }
}
